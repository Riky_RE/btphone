/***************************************************************************
*   Copyright (C) 2016 by Riccardo Redaelli, Fabiano Riccardi,            *
*   Andrea Salmoiraghi, Andrea Turri                                      *
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   As a special exception, if other files instantiate templates or use   *
*   macros or inline functions from this file, or you compile this file   *
*   and link it with other works to produce a work based on this file,    *
*   this file does not by itself cause the resulting work to be covered   *
*   by the GNU General Public License. However the source code for this   *
*   file must still be made available in accordance with the GNU General  *
*   Public License. This exception does not invalidate any other reasons  *
*   why a work based on this file might be covered by the GNU General     *
*   Public License.                                                       *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program; if not, see <http://www.gnu.org/licenses/>   *
***************************************************************************/

#ifndef CHAT_H
#define CHAT_H

#include <string>
#include "miosix.h"
#include "bluetooth.h"
#include "led/led.h"
#include <tr1/functional>

using namespace std::tr1;

/*
 * This class provide tools to instantiate a chat between two devices connected via bluetooth
 */
class Chat {
public:
        /*
         * Get the singleton of the class
         */
        static Chat& instance();
        /*
         * Check if bluetooth is connected otherwise try to do a connection and initialize the chat.
         */
        void init();
        void start();

        /*
         * True if there is an active connection
         */
        bool isActive();
        /*
         * True if there is an active connection and the chat is actually active
         */
        bool isChatting();
        /*
         * Send a string over the chat
         * #param data the string to send
         */
        void send(const string& data);

private:
        Chat();

        void receive(string data);
        void prepareToStart();
        static void* doEnter(void* arg);
        void enter();
        void prepareToQuit();
        static void* doQuit(void* arg);
        void quit();
        void chatOff();
        void terminate();
        void ringBell();

        FastMutex chatMutex;
        ConditionVariable chatCV;

        Bluetooth& bt;
        Led& led;
        bool chatting;
        bool active;

        //BTPHONE protocol
        static const string BTPHONE_PROTOCOL;
        static const string BTPHONE_RING;
        static const string BTPHONE_QUIT;
        static const string BTPHONE_DISCONNECT;
};

#endif
